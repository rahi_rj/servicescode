﻿using System;
using System.Collections.Generic;
using System.Text;
using ArcadisDemo.Entities;
using System.Threading.Tasks;
using System.Linq;

namespace ArcadisDemo.Repository
{
    public class DepartmentRepository: IDepartmentRepository
    {
        List<Department> departments;

        public DepartmentRepository()
        {
            departments = new List<Department>()
            {
                new Department{Id = 101, DeptName = "CSE", DeptHOD = "Rahul"},
                new Department{Id = 102, DeptName = "IT", DeptHOD = "Kumar"}
            };
        }

        /// <summary>
        /// method to add a new department
        /// </summary>
        /// <param name="dept"></param>
        /// <returns></returns>
        public async Task<bool> AddDept(Department dept)
        {
            var prevCount = departments.Count();
            departments.Add(
                new Department
                {
                    Id = dept.Id,
                    DeptName = dept.DeptName,
                    DeptHOD = dept.DeptHOD
                });
            var newCount = departments.Count();
            var result = newCount > prevCount ? true : false;
            return await Task.FromResult(result);
        }

        /// <summary>
        /// method to get the list of departments
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Department>> GetAllDept()
        {            
            return await Task.FromResult(departments);
        }

        /// <summary>
        /// method to update a department details
        /// </summary>
        /// <param name="deptId"></param>
        /// <param name="dept"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDept(int deptId, Department dept)
        {
            var _dept = departments.Find(d => d.Id == deptId);

            _dept.Id = dept.Id;
            _dept.DeptName = dept.DeptName;
            _dept.DeptHOD = dept.DeptHOD;

            var result = departments.Exists(d => d.DeptName == dept.DeptName) ? true : false;

            return await Task.FromResult(result);
        }

        /// <summary>
        /// method to delete a department
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDept(int deptId)
        {
            var _dept = departments.Find(d => d.Id == deptId);
            var result = departments.Remove(_dept);
            return await Task.FromResult(result);
        }
    }
}
