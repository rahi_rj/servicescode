﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ArcadisDemo.Entities;

namespace ArcadisDemo.Repository
{
    public interface IDepartmentRepository
    {
        Task<bool> AddDept(Department dept);

        Task<IEnumerable<Department>> GetAllDept();

        Task<bool> UpdateDept(int deptId, Department dept);

        Task<bool> DeleteDept(int deptId);
    }
}
