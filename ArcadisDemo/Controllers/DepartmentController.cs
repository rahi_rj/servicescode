﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ArcadisDemo.Entities;
using ArcadisDemo.Services;

namespace ArcadisDemo.Controllers
{
    [Route("api/department")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentUtility _departmentUtility;

        public DepartmentController(IDepartmentUtility departmentUtility)
        {
            _departmentUtility = departmentUtility;
        }

        /// <summary>
        /// method to get all departments
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getDepartments")]
        public async Task<IActionResult> GetDepartments()
        {
            try
            {
                var departments = await _departmentUtility.GetAllDept();
                if (departments == null)
                {
                    return NotFound();
                }

                return Ok(departments);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// API endpoint that creates a new department
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("addDepartment")]
        public async Task<IActionResult> AddDepartment([FromBody] Department dept)
        {
            return Ok(await _departmentUtility.AddDept(dept));
        }

        /// <summary>
        /// method to update the department
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("updateDepartment")]
        public async Task<IActionResult> UpdateDepartment(int id, [FromBody] Department dept)
        {
            return Ok(await _departmentUtility.UpdateDept(id,dept));
        }

        /// <summary>
        /// method to delete a department
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("deleteDepartment")]
        public async Task<IActionResult> DeleteDepartment(int id)
        {
            return Ok(await _departmentUtility.DeleteDept(id));
        }


    }
}