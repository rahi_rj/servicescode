﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArcadisDemo.Entities
{
    public class Department
    {
        #region Department Entity Properties
        public int Id { get; set; }

        public string DeptName { get; set; }

        public string DeptHOD { get; set; }
        #endregion
    }
}
