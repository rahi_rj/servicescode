﻿using System;
using System.Collections.Generic;
using System.Text;
using ArcadisDemo.Entities;
using ArcadisDemo.Repository;
using System.Threading.Tasks;

namespace ArcadisDemo.Services
{
    public class DepartmentUtility : IDepartmentUtility
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DepartmentUtility(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        /// <summary>
        /// method to add a department
        /// </summary>
        /// <param name="dept"></param>
        /// <returns></returns>
        public async Task<bool> AddDept(Department dept)
        {
            return await _departmentRepository.AddDept(dept);   
        }

        /// <summary>
        /// method to delete a department
        /// </summary>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteDept(int deptId)
        {
            return await _departmentRepository.DeleteDept(deptId);
        }

        /// <summary>
        /// method to get the department lists
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Department>> GetAllDept()
        {
            return await _departmentRepository.GetAllDept();
        }

        /// <summary>
        /// method to update the department data
        /// </summary>
        /// <param name="deptId"></param>
        /// <param name="dept"></param>
        /// <returns></returns>
        public async Task<bool> UpdateDept(int deptId, Department dept)
        {
            return await _departmentRepository.UpdateDept(deptId, dept);
        }
    }
}
