﻿using System;
using System.Collections.Generic;
using System.Text;
using ArcadisDemo.Entities;
using System.Threading.Tasks;

namespace ArcadisDemo.Services
{
    public interface IDepartmentUtility
    {
        Task<bool> AddDept(Department dept);

        Task<IEnumerable<Department>> GetAllDept();

        Task<bool> UpdateDept(int deptId, Department dept);

        Task<bool> DeleteDept(int deptId);
    }
}
